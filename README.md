# GOLang CRUD MySQL Sample

### Steps to do

1. Prepare and Import MySQL driver into your project

    Using Git Bash first install driver for Go's MySQL database package. Run below command and install MySQL driver's

    `go get -u github.com/go-sql-driver/mysql`
    
    Now create Goblog MySQL Database by this command :
    
    `create database Goblog`
    
2. Creating the Employee Table
    ```sql
    DROP TABLE IF EXISTS `employee`;
    CREATE TABLE `employee` (
      `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(30) NOT NULL,
      `city` varchar(30) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
    ```

3. Run the following command
   
   `go run main.go`
     
4. Load the following URL

    `http://localhost:8080/`
    
### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Data

![Add New Data](img/new.png "Add New Data")

Edit Data

![Edit Data](img/edit.png "Edit Data")
